from bs4 import BeautifulSoup
import requests

url = "https://psa-retail11-peugeot-qa.summit-automotive.solutions/configurable/finance/"

resp = requests.get(url, auth=('summit','summit'))
resp_soup = BeautifulSoup(resp.text, 'html.parser')

# brand=AP
print(50*"---")
price = resp_soup.find("span", {"class": "price"})
print(price)

print(50*"---")
priceLegalText = resp_soup.find("div", {"class": "priceLegalText"})
print(priceLegalText)

print(50*"---")
financeInfo = resp_soup.find("div", {"class": "vertical"})
print(financeInfo)