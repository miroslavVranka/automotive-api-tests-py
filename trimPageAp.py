from bs4 import BeautifulSoup
import requests

url = "https://psa-retail11-peugeot-qa.summit-automotive.solutions/trim/configurable/finance/nouveau-suv-2008-suv/"

resp = requests.get(url, auth=('summit','summit'))
resp_soup = BeautifulSoup(resp.text, 'html.parser')
# print(resp_soup.prettify())

# brand=AP
print(50*"---")
price = resp_soup.find("h2", {"class": "title title--detail"})
print(price)

print(50*"---")
equipmentSubtitle = resp_soup.find("div", {"class": "equipmentSubtitle"})
print(equipmentSubtitle)

print(50*"---")
financeInfo = resp_soup.find("div", {"class": "vertical"})
print(financeInfo)