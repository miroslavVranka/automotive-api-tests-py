import requests
import uuid
myToken = str(uuid.UUID)
myUrl = 'https://psa-retail11-citroen-qa.summit-automotive.solutions/configurable/finance/'
head = {'x-auth-token': 'token {}'.format(myToken)}
response = requests.get(myUrl, headers=head, auth=('summit', 'summit'))

print(response.status_code)
print(response.headers)