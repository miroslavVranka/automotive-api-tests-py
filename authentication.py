import requests
import uuid

url = "https://id-dcr-pre.citroen.com/login?openid.claimed_id=http://specs.openid.net/auth/2.0/identifier_select&openid.identity=http://specs.openid.net/auth/2.0/identifier_select&openid.mode=checkid_setup&openid.ns=http://specs.openid.net/auth/2.0&newcvs=1&goto=https://idpcvs-preprod.citroen.com/am/oauth2/authorize?client_id=a606de82-a90e-4732-aed4-30f8f19fa502&response_type=code&scope=openid%20implied_consent%20profile&redirect_uri=https://psa-retail11-citroen-qa.summit-automotive.solutions/digital-api/brandid/login&realm=clientsB2CCitroen&locale=fr-FR&state=%7B%22redirectTo%22%3A%22https%3A%2F%2Fpsa-retail11-citroen-qa.summit-automotive.solutions%2Foc-my-account%2Fmy_account%22%7D"

payload = 'formName=indexLogin&myProfileHidden=&newcvs=&email=retail_autotest_1@yopmail.com&password=Charlie11211&csrf_token=25e709e43b0b06708de8e1dcedce562&lang=fr&country=FR&captcha%5Bid%5D=187c9ee1474d8fb0282db94a4f1b4820&captcha%5Binput%5D=88my59&submit=VALIDER&remember=0'

clientID = uuid.UUID
headers = {
  'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0',
  'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
  'Accept-Language': 'en-US,en;q=0.5',
  'Content-Type': 'application/x-www-form-urlencoded',
  'Origin': 'https://id-dcr-pre.citroen.com',
  'Connection': 'keep-alive',
  'Referer': 'https://id-dcr-pre.citroen.com/login?openid.claimed_id=http://specs.openid.net/auth/2.0/identifier_select&openid.identity=http://specs.openid.net/auth/2.0/identifier_select&openid.mode=checkid_setup&openid.ns=http://specs.openid.net/auth/2.0&newcvs=1&goto=https://idpcvs-preprod.citroen.com/am/oauth2/authorize?client_id='+clientID+'&response_type=code&scope=openid%20implied_consent%20profile&redirect_uri=https://psa-retail11-citroen-qa.summit-automotive.solutions/digital-api/brandid/login&realm=clientsB2CCitroen&locale=fr-FR&state=%7B%22redirectTo%22%3A%22https%3A%2F%2Fpsa-retail11-citroen-qa.summit-automotive.solutions%2Foc-my-account%2Fmy_account%22%7D',
  'Cookie': 'PSACountry=US; DCROPENIDAC=dab95dfbe8664b991079bde815ea72f9; BIGipServerAPAC_DCR_FEND_PREPROD.app~APAC_DCR_FEND_PREPROD_pool=3264249866.20480.0000; _psac_gdpr_consent_purposes=[cat_ana][cat_com][cat_soc]; _gcl_au=1.1.2032465080.1595945697; ivbspd=4; _ga=GA1.2.1180684261.1595945698; _fbp=fb.1.1595945698070.469026716; _tag_frontend_42071_vid=4c559ead53a6d2f1b94655139b28016d6e9d1576f9d5edcf6c%3A99ea; _tag_frontend_42071_vid=4c559ead53a6d2f1b94655139b28016d6e9d1576f9d5edcf6c%3A99ea; _tag_frontend_42071_vid=4c559ead53a6d2f1b94655139b28016d6e9d1576f9d5edcf6c%3A99ea; _ga=GA1.3.1180684261.1595945698; _gid=GA1.2.1244837485.1596095201; _gid=GA1.3.1244837485.1596095201; _dc_gtm_UA-46309021-1=1; _dc_gtm_UA-45190795-1=1; _uetsid=f5030ba11df879748e23d6a8cb23fcd0; _uetvid=29433273fa1524ad1b8b046084b6de91; _gali=submit; _gat_UA-46309021-1=1; _gat_UA-45190795-1=1; _gali=submit',
  'Upgrade-Insecure-Requests': '1'
}

response = requests.request("POST", url, headers=headers, data = payload)


print(response.text.encode('utf8'))
