import random

import requests
import json

url = "https://preprod.citroen-uk-sol.psa-testing.summit-automotive.solutions/spc-api/api/v1/gb/en/AC/car-nameplates"

payload = {"aggregationParams": {
    "levelAggregations": [{"name": "nameplateBodyStyle", "nesting": ["nameplateBodyStyle"], "children": []}],
    "relevancyAggregations": [{"name": "prices.monthlyPrices.amount",
                               "fields": ["id", "model", "prices", "images", "lcdv16", "bodyStyle", "nameplate",
                                          "grGearbox", "grBodyStyle", "nameplateBodyStyle", "promotionalText",
                                          "pricesV2", "regularPaymentConditions", "noDeposit", "offers", "specPack",
                                          "nameplateBodyStyleSlug", "exteriorColourSlug", "interiorColourSlug",
                                          "specPackSlug", "engineGearboxFuelSlug", "interiorColour", "exteriorColour",
                                          "engine", "fuel", "gearbox", "title", "isDefaultConfiguration",
                                          "defaultConfiguration", "externalId"], "parent": "nameplateBodyStyle",
                               "operation": {"size": 1, "sort": "asc"}}]}, "filters": [
    {"nesting": ["prices", "type"], "name": "prices.basePrice", "operator": "EQUALS", "value": "Employee"},
    {"nesting": ["prices", "type"], "name": "prices.type", "operator": "EQUALS", "value": "Employee"},
    {"nesting": ["stock"], "name": "stock", "operator": "EQUALS", "value": "false"}], "extra": {"journey": "finance"}}

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0',
    'Accept': 'application/json',
    'Accept-Language': 'en-US,en;q=0.5',
    'Content-Type': 'application/json',
    'X-Auth-Token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTU5NjQ0Mjc4OCwiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiJiY2U5ZTcxNS1kNTYxLTExZWEtOTBlZC0zMGQ0OTY3NDgyMzIiLCJpYXQiOjE1OTY0NDI3ODgsImp0aSI6ImJjZTllNzE1LWQ1NjEtMTFlYS05MGVkLTMwZDQ5Njc0ODIzMiJ9.BhLUzvDZMyy_O_GDxei5ugEP3WxUsbtv6A4jFAdwHNI',
    'Origin': 'https://preprod.citroen-uk-sol.psa-testing.summit-automotive.solutions',
    'Connection': 'keep-alive',
    'Referer': 'https://preprod.citroen-uk-sol.psa-testing.summit-automotive.solutions/configurable/finance/',
    'Cookie': '__cfduid=d974fc8c4f7d9fe66b457b10c4bbee0c21594974320; toky_state=minimized; SAUT_SESSION_DS_qa=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTU5NjUzMTUxMiwiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiJhMDI5Njg1OS1kNjMwLTExZWEtYWVmMS0xODA1Y2YwMDkzZTAiLCJpYXQiOjE1OTY1MzE1MTIsImp0aSI6ImEwMjk2ODU5LWQ2MzAtMTFlYS1hZWYxLTE4MDVjZjAwOTNlMCJ9.tqcTaMBuwgoZXG_BdOEmsUITcCkKjcXqdl-q5GgVfoE; _ga=GA1.2.1412767318.1594974331; _gcl_au=1.1.1822209919.1594974332; _fbp=fb.1.1594974333419.362924355; SAUT_SESSION_AP_qa=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTU5NjE0MjE3NywiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiIyNGRlMjBlNy1kMmE2LTExZWEtYWVmMS0xODA1Y2YwMDkzZTAiLCJpYXQiOjE1OTYxNDIxNzcsImp0aSI6IjI0ZGUyMGU3LWQyYTYtMTFlYS1hZWYxLTE4MDVjZjAwOTNlMCJ9.HylWQHt7dHI5-GrxJY8OZpH_InVGTKf6ss_j0FABKJ4; SAUT_SESSION_AP_preprod=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTU5NTM0MDEyNCwiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiJiYzIxMmU4ZS1jYjVhLTExZWEtYWVmMS0xODA1Y2YwMDkzZTAiLCJpYXQiOjE1OTUzNDAxMjQsImp0aSI6ImJjMjEyZThlLWNiNWEtMTFlYS1hZWYxLTE4MDVjZjAwOTNlMCJ9.HFO97-A9Ie6Cj9MfANy4mhZsDhu5JTRElK7O0Efakdg; _dy_ses_load_seq=33415%3A1595340530871; _dy_c_exps=; _dy_soct=1019478.1034049.1595340530; _dycnst=dg; _dyid=-3494792433070836384; _dycst=dk.w.f.ss.; _dy_geo=CZ.EU.CZ_10.CZ_10_Prague; _dy_df_geo=Czech%20Republic..Prague; _dy_toffset=-1; _tag_frontend_15355_vid=08b389773cbe7c7af69bfa8c477fbd220cd75b120b7cabd5%3A6eb4; SAUT_SESSION_DS_preprod=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTU5NTM0MDQ2MiwiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiI4NWE5NjA2Ny1jYjViLTExZWEtYWVmMS0xODA1Y2YwMDkzZTAiLCJpYXQiOjE1OTUzNDA0NjIsImp0aSI6Ijg1YTk2MDY3LWNiNWItMTFlYS1hZWYxLTE4MDVjZjAwOTNlMCJ9.sW6AIr638C17qLLb0FXwXu-cgqRLGOs8inuDmYhpagE; _tag_frontend_82b15_vid=424de23309b27d530f31793933c0467c2d53bf0323f6abac%3A5858; _tag_frontend_42071_vid=4c559ead53a6d2f1b94655139b28016d6e9d1576f9d5edcf6c%3A99ea; cto_bundle=8lCDjF9Fc3ZyekxLcWR0ZGwlMkYyU0olMkZCTWpySGNzSGFvcGdvam9CazZueE1sOE5Gd21MJTJGN3JwMm14aHNSJTJCNlV1eFkzcUpRJTJCcklaZDNTN0lKTDA1elhYdnolMkJJYTFhZG5HcW1ZQVlwSGQzYWpvb1l1R2xjS3lwOG1sMTNCbWpMVkJQWm9kWm11SjZkZWY0Tk1hdFNIJTJGYVdFWUUydEhTTUkyZmFSNnY5bXUzd3k0ZXJJcjFzb0tqanpkVnlvajA2cUJ3VzFYb2FyT2xLZGQlMkJtVVp2ZndBczJQUFpUUSUzRCUzRA; _psac_gdpr_banner_id=0; _psac_gdpr_consent_purposes=[cat_ana][cat_com][cat_soc]; _psac_gdpr_consent_cookies=[Google Tag Manager][4w MarketPlace][Adara][Emetriq][IginitionOne (Netmining)][Smart Adserver][Xaxis][Adadyn (formerly Ozone Media)][Adyoulike][AppNexus][mPlatform][Netmining][Plista][Quantcast][Facebook][Ozone]; _psac_gdpr_consent_purposes_opposition=; _psac_gdpr_consent_given=1; ABTasty=uid=489ht2cmbkdtvaaj&fst=1595946740674&pst=1596111735062&cst=1596181464941&ns=8&pvt=28&pvis=28&th=; SAUT_SESSION_AC_preprod=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTU5NjE4NzMyMiwiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiJmYWI1NGU2Yy1kMzBlLTExZWEtOTBlZC0zMGQ0OTY3NDgyMzIiLCJpYXQiOjE1OTYxODczMjIsImp0aSI6ImZhYjU0ZTZjLWQzMGUtMTFlYS05MGVkLTMwZDQ5Njc0ODIzMiJ9.faaOyskfvjvuGCGLTzMsLh3y-LQDl0iFBJMTne2HlSA; SAUT_SESSION_AC_preprod=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTU5NjQ0Mjc4OCwiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiJiY2U5ZTcxNS1kNTYxLTExZWEtOTBlZC0zMGQ0OTY3NDgyMzIiLCJpYXQiOjE1OTY0NDI3ODgsImp0aSI6ImJjZTllNzE1LWQ1NjEtMTFlYS05MGVkLTMwZDQ5Njc0ODIzMiJ9.BhLUzvDZMyy_O_GDxei5ugEP3WxUsbtv6A4jFAdwHNI; SAUT_SESSION_AC_qa=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTU5NjUzMTQ1MCwiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiI3YjAzOWE4Mi1kNjMwLTExZWEtYWVmMS0xODA1Y2YwMDkzZTAiLCJpYXQiOjE1OTY1MzE0NTAsImp0aSI6IjdiMDM5YTgyLWQ2MzAtMTFlYS1hZWYxLTE4MDVjZjAwOTNlMCJ9.0mF-GiA7O4AA7wM-alhaRlc3XLHZv24c4XNMRFReJAU; _svtri=df48dad9-38c8-4bd8-893c-2dfb103813bd; _svs=%7B%22p%22%3A%7B%227%22%3A1596142616734%2C%223001%22%3A1596115547124%2C%224242%22%3A1596142616703%7D%7D; _gid=GA1.2.111991848.1596442790; ivbspd=3; SAUT_SESSION_AP_preprod=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTU5NjUzMTQ5MywiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiI5NGQ5ZjMxMi1kNjMwLTExZWEtYWVmMS0xODA1Y2YwMDkzZTAiLCJpYXQiOjE1OTY1MzE0OTMsImp0aSI6Ijk0ZDlmMzEyLWQ2MzAtMTFlYS1hZWYxLTE4MDVjZjAwOTNlMCJ9.vxA6FYhKZegh-PUqlA5NAJeeiRX4Ym3UdigZcx_cmTA; SAUT_SESSION_DS_preprod=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTU5NjUzMTU5MiwiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiJkMDA4YWQxNy1kNjMwLTExZWEtYWVmMS0xODA1Y2YwMDkzZTAiLCJpYXQiOjE1OTY1MzE1OTIsImp0aSI6ImQwMDhhZDE3LWQ2MzAtMTFlYS1hZWYxLTE4MDVjZjAwOTNlMCJ9.jJuXl6nTjF7xSHjQFlSo4E11SufO13-sUN1nMIdoYO0; _uetsid=42059f5f979cf7924811f7192780ff51; _uetvid=34f9a87bbfbb346e4e4840069eecd020; SAUT_SESSION_AC_qa=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTU5NzE0NTQ5MywiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiIyNTk2NDU2MC1kYmM2LTExZWEtYWVmMS0xODA1Y2YwMDkzZTAiLCJpYXQiOjE1OTcxNDU0OTMsImp0aSI6IjI1OTY0NTYwLWRiYzYtMTFlYS1hZWYxLTE4MDVjZjAwOTNlMCJ9.Vqc0m_8tsnB2CsA3SJ3v9jtLgiv6iU2QzsakAlVff-E; SAUT_SESSION_AC_preprod=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTU5NjUzOTEwNywiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiI0ZjE4YjA4Ny1kNjQyLTExZWEtYWVmMS0xODA1Y2YwMDkzZTAiLCJpYXQiOjE1OTY1MzkxMDcsImp0aSI6IjRmMThiMDg3LWQ2NDItMTFlYS1hZWYxLTE4MDVjZjAwOTNlMCJ9.IPudGB5P46y86PnI4j6PIuu8n_hDStwZLWqHenXn-I8',
    'TE': 'Trailers'
}

response = requests.request("POST", url, headers=headers, json=payload)
print('Response of /spc-api/api/v1/gb/en/AC/car-nameplates -> '+ str(response.status_code))
body = json.loads(response.text)

items = body['items']
nameplates = []
for i in items:
    for cars in i['items']['prices.monthlyPrices.amount']:
        nameplates.append(cars['nameplateBodyStyleSlug'])
print(nameplates)
# nameplate = random.choice(nameplates) 2356
nameplate = nameplates[10]
print('pick nameplate -> '+ nameplate)

url = "https://preprod.citroen-uk-sol.psa-testing.summit-automotive.solutions/spc-api/api/v1/gb/en/AC/car-details-list"

payload = {
    "aggregationParams": {
        "levelAggregations": [
            {
                "name": "detailsAggregated",
                "nesting": [
                    "detailsAggregated"
                ],
                "children": []
            }
        ],
        "relevancyAggregations": [
            {
                "name": "prices.basePrice",
                "fields": [
                    "exteriorColour",
                    "exteriorColourSlug",
                    "engine",
                    "specPackSlug",
                    "engineGearboxFuelSlug",
                    "gearbox",
                    "interiorColour",
                    "interiorColourSlug",
                    "prices",
                    "nameplateBodyStyleSlug",
                    "externalId"
                ],
                "parent": "detailsAggregated",
                "operation": {
                    "size": 1,
                    "sort": "recommended"
                }
            }
        ]
    },
    "filters": [
        {
            "nesting": [
                "nameplateBodyStyleSlug"
            ],
            "name": "nameplateBodyStyleSlug",
            "operator": "EQUALS",
            "value": nameplate,
            "parent": None
        },
        {
            "nesting": [
                "prices",
                "monthlyPrices",
                "amount"
            ],
            "name": "prices.monthlyPrices.amount.global",
            "operator": "BETWEEN",
            "value": {
                "from": 1,
                "to": 99999
            },
            "parent": None
        },
        {
            "nesting": [
                "prices",
                "type"
            ],
            "name": "prices.type",
            "operator": "EQUALS",
            "value": "Employee"
        },
        {
            "nesting": [
                "stock"
            ],
            "name": "stock",
            "operator": "EQUALS",
            "value": "false"
        }
    ],
    "extra": {
        "journey": "finance"
    }
}

headers = {
    'authority': 'preprod.citroen-uk-sol.psa-testing.summit-automotive.solutions',
    'authorization': 'Basic c3VtbWl0OnN1bW1pdA==',
    'accept': 'application/json',
    'x-auth-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTU5NzY5ODc2MSwiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiI1MjFhMDNkZi1lMGNlLTExZWEtYWUxMy1mMDhiZTllY2U0YWYiLCJpYXQiOjE1OTc2OTg3NjEsImp0aSI6IjUyMWEwM2RmLWUwY2UtMTFlYS1hZTEzLWYwOGJlOWVjZTRhZiJ9.AIi5XWjV8t5w-wEGiyvkaxCmaHSmAPC7T6ToAYsdnRY',
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36',
    'content-type': 'application/json',
    'origin': 'https://preprod.citroen-uk-sol.psa-testing.summit-automotive.solutions',
    'sec-fetch-site': 'same-origin',
    'sec-fetch-mode': 'cors',
    'sec-fetch-dest': 'empty',
    'referer': 'https://preprod.citroen-uk-sol.psa-testing.summit-automotive.solutions/configurable/finance/',
    'accept-language': 'sk-SK,sk;q=0.9,cs-CZ;q=0.8,cs;q=0.7,en-GB;q=0.6,en;q=0.5,en-US;q=0.4',
    'cookie': '_ga=GA1.2.263015739.1592909227; _gcl_au=1.1.1675330784.1592909254; _fbp=fb.1.1592909254301.1832268138; toky_closedByUser=true; _tag_frontend_42071_vid=7f22d389513f8c127523644a5fa397b837fbbfc92eb0088d%3Aa710; _dy_c_exps=; _dycnst=dg; _dyid=3976679335851911403; _tag_frontend_15355_vid=d20f6be4daa5a483519d19d603721e3a408fce201f524aedaa%3A6422; SAUT_SESSION_DS_preprod=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTU5NTMzOTAxMCwiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiIyNDQ1MTAwMC1jYjU4LTExZWEtYWVmMS0xODA1Y2YwMDkzZTAiLCJpYXQiOjE1OTUzMzkwMTAsImp0aSI6IjI0NDUxMDAwLWNiNTgtMTFlYS1hZWYxLTE4MDVjZjAwOTNlMCJ9.UAmoUGNyz45JBw4JvDqanxhIxIB2Maf3ljaNutH4yDw; _tag_frontend_82b15_vid=3f0b893c181074177aeb9f134549bda36e1f06e79956f639%3A996b; __cfduid=d725e4b9cd1c2c2b46953fa19ea0df8581595494685; _dycst=dk.w.c.ws.; _dy_geo=SK.EU.SK_PV.SK_PV_Pre%C5%A1ov; _dy_df_geo=Slovakia..Pre%C5%A1ov; _dy_toffset=-1; _psac_gdpr_banner_id=0; _psac_gdpr_consent_purposes_opposition=; _psac_gdpr_consent_purposes=[cat_ana][cat_com][cat_soc]; _psac_gdpr_consent_given=1; _psac_gdpr_consent_cookies=[Google Tag Manager][4w MarketPlace][Adara][Emetriq][IginitionOne (Netmining)][Smart Adserver][Xaxis][Adadyn (formerly Ozone Media)][Adyoulike][AppNexus][mPlatform][Netmining][Plista][Quantcast][Facebook][Ozone]; SAUT_SESSION_AP_preprod=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTU5NjYzMzE2MywiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiI0YzQ2NzQ2Zi1kNzFkLTExZWEtYWVmMS0xODA1Y2YwMDkzZTAiLCJpYXQiOjE1OTY2MzMxNjMsImp0aSI6IjRjNDY3NDZmLWQ3MWQtMTFlYS1hZWYxLTE4MDVjZjAwOTNlMCJ9.SjP5e-nk1bIggcJ9IKTROJbH_5Ah2ifuFlTRCUguGn4; _dy_ses_load_seq=50742%3A1596633168508; _dy_soct=1019478.1034049.1596633168; toky_state=minimized; SAUT_SESSION_AC_qa=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTU5Njc4NjE3NSwiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiIzMDU5OWU0OC1kODgxLTExZWEtOTBlZC0zMGQ0OTY3NDgyMzIiLCJpYXQiOjE1OTY3ODYxNzUsImp0aSI6IjMwNTk5ZTQ4LWQ4ODEtMTFlYS05MGVkLTMwZDQ5Njc0ODIzMiJ9.G0KqcVY8NPrFPpxPCRLLvkSeQ_tZoWu208vZV38BUPM; SAUT_SESSION_AC_preprod=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTU5NzA0ODYwNywiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiIyYTUyYTc4MS1kYWU0LTExZWEtOTBlZC0zMGQ0OTY3NDgyMzIiLCJpYXQiOjE1OTcwNDg2MDcsImp0aSI6IjJhNTJhNzgxLWRhZTQtMTFlYS05MGVkLTMwZDQ5Njc0ODIzMiJ9.81PFQe6PQJHkEbuvliCFBm4wk6tSW6E6QW9vVXWptMI; SAUT_SESSION_SOLUK_AC_preprod=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTU5NzI2Njk3NywiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiI4Zjg1ZTJjMi1kY2UwLTExZWEtOTBlZC0zMGQ0OTY3NDgyMzIiLCJpYXQiOjE1OTcyNjY5NzcsImp0aSI6IjhmODVlMmMyLWRjZTAtMTFlYS05MGVkLTMwZDQ5Njc0ODIzMiJ9.cqEZkIJnqRb_qgLXNdnpB2MEbQ_YLrcPSZ3IKOsv4pc; SAUT_SESSION_DS_qa=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTU5NzI3MjIwMSwiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiIyOGE1ZTVlMC1kY2VkLTExZWEtYWVmMS0xODA1Y2YwMDkzZTAiLCJpYXQiOjE1OTcyNzIyMDEsImp0aSI6IjI4YTVlNWUwLWRjZWQtMTFlYS1hZWYxLTE4MDVjZjAwOTNlMCJ9.b7F_3fJv-yExO9umIU8vImV0e-eT2FQqxcvHqZD-Cpc; SAUT_SESSION_SOLUK_AC_preprod=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTU5NzY0ODMzNiwiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiJlZDA0MzEyMi1lMDU4LTExZWEtYWUxMy1mMDhiZTllY2U0YWYiLCJpYXQiOjE1OTc2NDgzMzYsImp0aSI6ImVkMDQzMTIyLWUwNTgtMTFlYS1hZTEzLWYwOGJlOWVjZTRhZiJ9.jxKtt1-g06faE4qDhypt5G2cOmI2jg485dq-2uC08lk; _gid=GA1.2.1405044407.1597648338; SAUT_SESSION_AC_preprod=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTU5NzY0ODYzOSwiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiJhZjM3NzhlNS1lMDU5LTExZWEtYTc0Zi00YjgwOWU5YjJjMGQiLCJpYXQiOjE1OTc2NDg2MzksImp0aSI6ImFmMzc3OGU1LWUwNTktMTFlYS1hNzRmLTRiODA5ZTliMmMwZCJ9.Sy49TYxVt5sgd_A_U15ETzLQKZwZqjxYIYoKx-qKp1s; _uetsid=aa80070db4a8966bc6dbf321f64e2286; _uetvid=790c1d34ae24f65e1ab7718c7204a04b; SAUT_SESSION_AC_qa=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTU5NzE0NTQ5MywiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiIyNTk2NDU2MC1kYmM2LTExZWEtYWVmMS0xODA1Y2YwMDkzZTAiLCJpYXQiOjE1OTcxNDU0OTMsImp0aSI6IjI1OTY0NTYwLWRiYzYtMTFlYS1hZWYxLTE4MDVjZjAwOTNlMCJ9.Vqc0m_8tsnB2CsA3SJ3v9jtLgiv6iU2QzsakAlVff-E; SAUT_SESSION_AC_preprod=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTU5NzA1MzM0MSwiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiIzMDMxODJmNS1kYWVmLTExZWEtOTBlZC0zMGQ0OTY3NDgyMzIiLCJpYXQiOjE1OTcwNTMzNDEsImp0aSI6IjMwMzE4MmY1LWRhZWYtMTFlYS05MGVkLTMwZDQ5Njc0ODIzMiJ9.UIetgarcW1QfevctzdkwlLYpEoMoTM5o-59mCt1sxMQ'
}

response = requests.request("POST", url, headers=headers, json=payload)
print("*"*80)
print('Response of /spc-api/api/v1/gb/en/AC/car-details-list aggregated-> '+ str(response.status_code))
# while (response.status_code is not 200):
#     print(response.headers)
body = json.loads(response.content)
pretty_json = None
externalIds = []
items = body['items'][0]['items']
for a in items:
    for b in a['items']['prices.basePrice']:
        for extColorItemPrice in b['exteriorColour']['pricesV2']:
            for intColorItemPrice in b['interiorColour']['pricesV2']:
                if extColorItemPrice['finalPriceInclTax'] > 0 and extColorItemPrice['type'] == "B2C_Finance" \
                        and intColorItemPrice['type'] == "B2C_Finance" and intColorItemPrice['finalPriceInclTax'] > 0:
                    print(b['externalId'])
