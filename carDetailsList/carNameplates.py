import json

import requests

url = "https://preprod.citroen-uk-sol.psa-testing.summit-automotive.solutions/spc-api/api/v1/gb/en/AC/car-nameplates"

payload = {"aggregationParams": {
    "levelAggregations": [{"name": "nameplateBodyStyle", "nesting": ["nameplateBodyStyle"], "children": []}],
    "relevancyAggregations": [{"name": "prices.monthlyPrices.amount",
                               "fields": ["id", "model", "prices", "images", "lcdv16", "bodyStyle", "nameplate",
                                          "grGearbox", "grBodyStyle", "nameplateBodyStyle", "promotionalText",
                                          "pricesV2", "regularPaymentConditions", "noDeposit", "offers", "specPack",
                                          "nameplateBodyStyleSlug", "exteriorColourSlug", "interiorColourSlug",
                                          "specPackSlug", "engineGearboxFuelSlug", "interiorColour", "exteriorColour",
                                          "engine", "fuel", "gearbox", "title", "isDefaultConfiguration",
                                          "defaultConfiguration", "externalId"], "parent": "nameplateBodyStyle",
                               "operation": {"size": 1, "sort": "asc"}}]}, "filters": [
    {"nesting": ["prices", "type"], "name": "prices.basePrice", "operator": "EQUALS", "value": "Employee"},
    {"nesting": ["prices", "type"], "name": "prices.type", "operator": "EQUALS", "value": "Employee"},
    {"nesting": ["stock"], "name": "stock", "operator": "EQUALS", "value": "false"}], "extra": {"journey": "finance"}}
headers = {
    'authority': 'preprod.citroen-uk-sol.psa-testing.summit-automotive.solutions',
    'authorization': 'Basic c3VtbWl0OnN1bW1pdA==',
    'accept': 'application/json',
    'x-auth-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTU5NzY0ODMzNiwiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiJlZDA0MzEyMi1lMDU4LTExZWEtYWUxMy1mMDhiZTllY2U0YWYiLCJpYXQiOjE1OTc2NDgzMzYsImp0aSI6ImVkMDQzMTIyLWUwNTgtMTFlYS1hZTEzLWYwOGJlOWVjZTRhZiJ9.jxKtt1-g06faE4qDhypt5G2cOmI2jg485dq-2uC08lk',
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36',
    'content-type': 'application/json',
    'origin': 'https://preprod.citroen-uk-sol.psa-testing.summit-automotive.solutions',
    'sec-fetch-site': 'same-origin',
    'sec-fetch-mode': 'cors',
    'sec-fetch-dest': 'empty',
    'referer': 'https://preprod.citroen-uk-sol.psa-testing.summit-automotive.solutions/configurable/finance/',
    'accept-language': 'sk-SK,sk;q=0.9,cs-CZ;q=0.8,cs;q=0.7,en-GB;q=0.6,en;q=0.5,en-US;q=0.4',
    'cookie': '_ga=GA1.2.263015739.1592909227; _gcl_au=1.1.1675330784.1592909254; _fbp=fb.1.1592909254301.1832268138; toky_closedByUser=true; _tag_frontend_42071_vid=7f22d389513f8c127523644a5fa397b837fbbfc92eb0088d%3Aa710; _dy_c_exps=; _dycnst=dg; _dyid=3976679335851911403; _tag_frontend_15355_vid=d20f6be4daa5a483519d19d603721e3a408fce201f524aedaa%3A6422; SAUT_SESSION_DS_preprod=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTU5NTMzOTAxMCwiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiIyNDQ1MTAwMC1jYjU4LTExZWEtYWVmMS0xODA1Y2YwMDkzZTAiLCJpYXQiOjE1OTUzMzkwMTAsImp0aSI6IjI0NDUxMDAwLWNiNTgtMTFlYS1hZWYxLTE4MDVjZjAwOTNlMCJ9.UAmoUGNyz45JBw4JvDqanxhIxIB2Maf3ljaNutH4yDw; _tag_frontend_82b15_vid=3f0b893c181074177aeb9f134549bda36e1f06e79956f639%3A996b; __cfduid=d725e4b9cd1c2c2b46953fa19ea0df8581595494685; _dycst=dk.w.c.ws.; _dy_geo=SK.EU.SK_PV.SK_PV_Pre%C5%A1ov; _dy_df_geo=Slovakia..Pre%C5%A1ov; _dy_toffset=-1; _psac_gdpr_banner_id=0; _psac_gdpr_consent_purposes_opposition=; _psac_gdpr_consent_purposes=[cat_ana][cat_com][cat_soc]; _psac_gdpr_consent_given=1; _psac_gdpr_consent_cookies=[Google Tag Manager][4w MarketPlace][Adara][Emetriq][IginitionOne (Netmining)][Smart Adserver][Xaxis][Adadyn (formerly Ozone Media)][Adyoulike][AppNexus][mPlatform][Netmining][Plista][Quantcast][Facebook][Ozone]; SAUT_SESSION_AP_preprod=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTU5NjYzMzE2MywiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiI0YzQ2NzQ2Zi1kNzFkLTExZWEtYWVmMS0xODA1Y2YwMDkzZTAiLCJpYXQiOjE1OTY2MzMxNjMsImp0aSI6IjRjNDY3NDZmLWQ3MWQtMTFlYS1hZWYxLTE4MDVjZjAwOTNlMCJ9.SjP5e-nk1bIggcJ9IKTROJbH_5Ah2ifuFlTRCUguGn4; _dy_ses_load_seq=50742%3A1596633168508; _dy_soct=1019478.1034049.1596633168; toky_state=minimized; SAUT_SESSION_AC_qa=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTU5Njc4NjE3NSwiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiIzMDU5OWU0OC1kODgxLTExZWEtOTBlZC0zMGQ0OTY3NDgyMzIiLCJpYXQiOjE1OTY3ODYxNzUsImp0aSI6IjMwNTk5ZTQ4LWQ4ODEtMTFlYS05MGVkLTMwZDQ5Njc0ODIzMiJ9.G0KqcVY8NPrFPpxPCRLLvkSeQ_tZoWu208vZV38BUPM; SAUT_SESSION_AC_preprod=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTU5NzA0ODYwNywiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiIyYTUyYTc4MS1kYWU0LTExZWEtOTBlZC0zMGQ0OTY3NDgyMzIiLCJpYXQiOjE1OTcwNDg2MDcsImp0aSI6IjJhNTJhNzgxLWRhZTQtMTFlYS05MGVkLTMwZDQ5Njc0ODIzMiJ9.81PFQe6PQJHkEbuvliCFBm4wk6tSW6E6QW9vVXWptMI; SAUT_SESSION_SOLUK_AC_preprod=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTU5NzI2Njk3NywiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiI4Zjg1ZTJjMi1kY2UwLTExZWEtOTBlZC0zMGQ0OTY3NDgyMzIiLCJpYXQiOjE1OTcyNjY5NzcsImp0aSI6IjhmODVlMmMyLWRjZTAtMTFlYS05MGVkLTMwZDQ5Njc0ODIzMiJ9.cqEZkIJnqRb_qgLXNdnpB2MEbQ_YLrcPSZ3IKOsv4pc; SAUT_SESSION_DS_qa=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTU5NzI3MjIwMSwiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiIyOGE1ZTVlMC1kY2VkLTExZWEtYWVmMS0xODA1Y2YwMDkzZTAiLCJpYXQiOjE1OTcyNzIyMDEsImp0aSI6IjI4YTVlNWUwLWRjZWQtMTFlYS1hZWYxLTE4MDVjZjAwOTNlMCJ9.b7F_3fJv-yExO9umIU8vImV0e-eT2FQqxcvHqZD-Cpc; SAUT_SESSION_SOLUK_AC_preprod=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTU5NzY0ODMzNiwiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiJlZDA0MzEyMi1lMDU4LTExZWEtYWUxMy1mMDhiZTllY2U0YWYiLCJpYXQiOjE1OTc2NDgzMzYsImp0aSI6ImVkMDQzMTIyLWUwNTgtMTFlYS1hZTEzLWYwOGJlOWVjZTRhZiJ9.jxKtt1-g06faE4qDhypt5G2cOmI2jg485dq-2uC08lk; _gid=GA1.2.1405044407.1597648338; SAUT_SESSION_AC_preprod=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTU5NzY0ODYzOSwiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiJhZjM3NzhlNS1lMDU5LTExZWEtYTc0Zi00YjgwOWU5YjJjMGQiLCJpYXQiOjE1OTc2NDg2MzksImp0aSI6ImFmMzc3OGU1LWUwNTktMTFlYS1hNzRmLTRiODA5ZTliMmMwZCJ9.Sy49TYxVt5sgd_A_U15ETzLQKZwZqjxYIYoKx-qKp1s; _uetsid=aa80070db4a8966bc6dbf321f64e2286; _uetvid=790c1d34ae24f65e1ab7718c7204a04b; SAUT_SESSION_AC_qa=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTU5NzE0NTQ5MywiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiIyNTk2NDU2MC1kYmM2LTExZWEtYWVmMS0xODA1Y2YwMDkzZTAiLCJpYXQiOjE1OTcxNDU0OTMsImp0aSI6IjI1OTY0NTYwLWRiYzYtMTFlYS1hZWYxLTE4MDVjZjAwOTNlMCJ9.Vqc0m_8tsnB2CsA3SJ3v9jtLgiv6iU2QzsakAlVff-E; SAUT_SESSION_AC_preprod=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTU5NzA1MzM0MSwiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiIzMDMxODJmNS1kYWVmLTExZWEtOTBlZC0zMGQ0OTY3NDgyMzIiLCJpYXQiOjE1OTcwNTMzNDEsImp0aSI6IjMwMzE4MmY1LWRhZWYtMTFlYS05MGVkLTMwZDQ5Njc0ODIzMiJ9.UIetgarcW1QfevctzdkwlLYpEoMoTM5o-59mCt1sxMQ'
}

response = requests.request("POST", url, headers=headers, json=payload)
print(response.status_code)
print(response.headers)
body = json.loads(response.text)

items = body['items']
for i in items:
    for cars in i['items']['prices.monthlyPrices.amount']:
        print(cars['nameplateBodyStyleSlug'])
