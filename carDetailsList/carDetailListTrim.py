import json

import requests

url = "https://preprod.citroen-uk-sol.psa-testing.summit-automotive.solutions/spc-api/api/v1/gb/en/AC/car-details-list"

payload = {"aggregationParams": {"levelAggregations": [
    {"name": "detailsOptionsAggregated", "nesting": ["detailsOptionsAggregated"], "children": []}],
    "relevancyAggregations": [{"name": "prices.monthlyPrices.amount", "fields": ["*"],
                               "parent": "detailsOptionsAggregated",
                               "operation": {"size": 1, "sort": "asc"}}]}, "filters": [
    {"nesting": ["nameplateBodyStyleSlug"], "name": "nameplateBodyStyleSlug", "operator": "EQUALS",
     "value": "new-c3-5-door"},
    {"nesting": ["specPackSlug"], "name": "specPackSlug", "operator": "EQUALS", "value": "flair-plus"},
    {"nesting": ["stock"], "name": "stock", "operator": "EQUALS", "value": "false"}], "extra": {"journey": "finance"}}
headers = {
    'authority': 'preprod.citroen-uk-sol.psa-testing.summit-automotive.solutions',
    'authorization': 'Basic c3VtbWl0OnN1bW1pdA==',
    'accept': 'application/json',
    'x-auth-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTU5ODI1MDY5OCwiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiI0YjEyNWI1YS1lNWQzLTExZWEtYWUxMy1mMDhiZTllY2U0YWYiLCJpYXQiOjE1OTgyNTA2OTgsImp0aSI6IjRiMTI1YjVhLWU1ZDMtMTFlYS1hZTEzLWYwOGJlOWVjZTRhZiJ9.8uYkLVnlaetpS67hQj2d6gHgX995PiIATz3sWpCPdY4',
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36',
    'content-type': 'application/json',
    'origin': 'https://preprod.citroen-uk-sol.psa-testing.summit-automotive.solutions',
    'sec-fetch-site': 'same-origin',
    'sec-fetch-mode': 'cors',
    'sec-fetch-dest': 'empty',
    'referer': 'https://preprod.citroen-uk-sol.psa-testing.summit-automotive.solutions/trim/configurable/finance/new-c3-5-door/',
    'accept-language': 'sk-SK,sk;q=0.9,cs-CZ;q=0.8,cs;q=0.7,en-GB;q=0.6,en;q=0.5,en-US;q=0.4',
    'cookie': '_ga=GA1.2.263015739.1592909227; _gcl_au=1.1.1675330784.1592909254; _fbp=fb.1.1592909254301.1832268138; toky_closedByUser=true; _tag_frontend_42071_vid=7f22d389513f8c127523644a5fa397b837fbbfc92eb0088d%3Aa710; _dy_c_exps=; _dycnst=dg; _dyid=3976679335851911403; _tag_frontend_15355_vid=d20f6be4daa5a483519d19d603721e3a408fce201f524aedaa%3A6422; _tag_frontend_82b15_vid=3f0b893c181074177aeb9f134549bda36e1f06e79956f639%3A996b; _dycst=dk.w.c.ws.; _dy_geo=SK.EU.SK_PV.SK_PV_Pre%C5%A1ov; _dy_df_geo=Slovakia..Pre%C5%A1ov; _dy_toffset=-1; _psac_gdpr_banner_id=0; _psac_gdpr_consent_purposes_opposition=; _psac_gdpr_consent_purposes=[cat_ana][cat_com][cat_soc]; _psac_gdpr_consent_given=1; _psac_gdpr_consent_cookies=[Google Tag Manager][4w MarketPlace][Adara][Emetriq][IginitionOne (Netmining)][Smart Adserver][Xaxis][Adadyn (formerly Ozone Media)][Adyoulike][AppNexus][mPlatform][Netmining][Plista][Quantcast][Facebook][Ozone]; SAUT_SESSION_AP_preprod=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTU5NjYzMzE2MywiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiI0YzQ2NzQ2Zi1kNzFkLTExZWEtYWVmMS0xODA1Y2YwMDkzZTAiLCJpYXQiOjE1OTY2MzMxNjMsImp0aSI6IjRjNDY3NDZmLWQ3MWQtMTFlYS1hZWYxLTE4MDVjZjAwOTNlMCJ9.SjP5e-nk1bIggcJ9IKTROJbH_5Ah2ifuFlTRCUguGn4; _dy_ses_load_seq=50742%3A1596633168508; _dy_soct=1019478.1034049.1596633168; toky_state=minimized; SAUT_SESSION_AC_preprod=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTU5NzA0ODYwNywiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiIyYTUyYTc4MS1kYWU0LTExZWEtOTBlZC0zMGQ0OTY3NDgyMzIiLCJpYXQiOjE1OTcwNDg2MDcsImp0aSI6IjJhNTJhNzgxLWRhZTQtMTFlYS05MGVkLTMwZDQ5Njc0ODIzMiJ9.81PFQe6PQJHkEbuvliCFBm4wk6tSW6E6QW9vVXWptMI; SAUT_SESSION_AC_dev=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTU5NzY0ODYzOSwiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiJhZjM3NzhlNS1lMDU5LTExZWEtYTc0Zi00YjgwOWU5YjJjMGQiLCJpYXQiOjE1OTc2NDg2MzksImp0aSI6ImFmMzc3OGU1LWUwNTktMTFlYS1hNzRmLTRiODA5ZTliMmMwZCJ9.Sy49TYxVt5sgd_A_U15ETzLQKZwZqjxYIYoKx-qKp1s; SAUT_SESSION_SOLUK_AC_qa=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTU5Nzc0MTk3NywiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiJlZWEyMWU5NS1lMTMyLTExZWEtYWUxMy1mMDhiZTllY2U0YWYiLCJpYXQiOjE1OTc3NDE5NzcsImp0aSI6ImVlYTIxZTk1LWUxMzItMTFlYS1hZTEzLWYwOGJlOWVjZTRhZiJ9.TlvWveMz5dKO2zWhTYI8d1YRoAuZm3gMHce9h5aC2mA; SAUT_SESSION_DS_qa=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTU5NzgyNDcxNCwiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiJhM2Q1MGFhNC1lMWYzLTExZWEtYTc0Zi00YjgwOWU5YjJjMGQiLCJpYXQiOjE1OTc4MjQ3MTQsImp0aSI6ImEzZDUwYWE0LWUxZjMtMTFlYS1hNzRmLTRiODA5ZTliMmMwZCJ9.cc9cvoz5M8j2GqJA2BRhIRjLrt0Fy34Voub9Eal1Tzg; SAUT_SESSION_AP_qa=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTU5NzgyNzg4MywiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiIwNTA2MmEwYi1lMWZiLTExZWEtYTc0Zi00YjgwOWU5YjJjMGQiLCJpYXQiOjE1OTc4Mjc4ODMsImp0aSI6IjA1MDYyYTBiLWUxZmItMTFlYS1hNzRmLTRiODA5ZTliMmMwZCJ9.2d6TLD2aepMOWVHo0drNIRwY3CgKl_D-BQIZDfS0pTU; _ga=GA1.5.263015739.1592909227; MOP_JOURNEY=finance; __cfduid=d33ab90654a90f408c80124ae85d952881598250690; SAUT_SESSION_SOLUK_AC_preprod=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTU5ODI1MDY5OCwiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiI0YjEyNWI1YS1lNWQzLTExZWEtYWUxMy1mMDhiZTllY2U0YWYiLCJpYXQiOjE1OTgyNTA2OTgsImp0aSI6IjRiMTI1YjVhLWU1ZDMtMTFlYS1hZTEzLWYwOGJlOWVjZTRhZiJ9.8uYkLVnlaetpS67hQj2d6gHgX995PiIATz3sWpCPdY4; _gid=GA1.2.270915691.1598250706; SAUT_SESSION_SOLUK_AC_dev=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTU5ODI1Mzc3NiwiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiI3NWM1ZmYyMC1lNWRhLTExZWEtYWUxMy1mMDhiZTllY2U0YWYiLCJpYXQiOjE1OTgyNTM3NzYsImp0aSI6Ijc1YzVmZjIwLWU1ZGEtMTFlYS1hZTEzLWYwOGJlOWVjZTRhZiJ9.A-0lAe3DjOx4Zbn9HfPsETm22d0R1J7jFg2sC7WKb8o; SAUT_SESSION_AC_qa=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTU5ODI1NDQzMywiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiIxZDdlN2FiYS1lNWRjLTExZWEtYWI1Ni1jNGZiOWUyNGRhZDgiLCJpYXQiOjE1OTgyNTQ0MzMsImp0aSI6IjFkN2U3YWJhLWU1ZGMtMTFlYS1hYjU2LWM0ZmI5ZTI0ZGFkOCJ9.-3In-3acdOdMJyRxP7Lq2FDJR_4RWWXGSyNJndIgJJM; _psac_gdpr_stamp=1; MOP_ID=NWY0M2JlYWYzMDdiMWUzMTlkNzM2ZGYx; MOP_CAR_CONFIGURATION=1CB6A5RNMJT0A040+0MM00NEU+0PV00RFC; _uetsid=ab5ae1f713e065f0d4b99f40bb1ec678; _uetvid=790c1d34ae24f65e1ab7718c7204a04b; SAUT_SESSION_AC_qa=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTU5NzE0NTQ5MywiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiIyNTk2NDU2MC1kYmM2LTExZWEtYWVmMS0xODA1Y2YwMDkzZTAiLCJpYXQiOjE1OTcxNDU0OTMsImp0aSI6IjI1OTY0NTYwLWRiYzYtMTFlYS1hZWYxLTE4MDVjZjAwOTNlMCJ9.Vqc0m_8tsnB2CsA3SJ3v9jtLgiv6iU2QzsakAlVff-E; SAUT_SESSION_AC_dev=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTU5NjUzOTEwNywiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiI0ZjE4YjA4Ny1kNjQyLTExZWEtYWVmMS0xODA1Y2YwMDkzZTAiLCJpYXQiOjE1OTY1MzkxMDcsImp0aSI6IjRmMThiMDg3LWQ2NDItMTFlYS1hZWYxLTE4MDVjZjAwOTNlMCJ9.IPudGB5P46y86PnI4j6PIuu8n_hDStwZLWqHenXn-I8'
}

response = requests.request("POST", url, headers=headers, json=payload)
body = json.loads(response.text)
pretty_json = None
items = body['items'][0]['items']
for item in items:
    extItem = item['items']['prices.monthlyPrices.amount'][0]['exteriorColour']['pricesV2']
    intItem = item['items']['prices.monthlyPrices.amount'][0]['interiorColour']['pricesV2']
    for extItemPrice in extItem:
        for intItemPrice in intItem:
            if extItemPrice['finalPriceInclTax'] > 0 and extItemPrice['type'] == "B2C_Finance" \
                    and intItemPrice['type'] == "B2C_Finance" and intItemPrice['finalPriceInclTax'] > 0:
                print(json.dumps(item['items']['prices.monthlyPrices.amount'][0]['externalId'], indent=2))
                print(len(item['items']['prices.monthlyPrices.amount'][0]['externalId']))
